namespace REstate.Configuration
{
    public enum CodeTypes
    {
        SqlScalarBool = 1,
        SqlScalarInt = 2,
        SqlAction = 3,
        ScriptPredicate = 4,
        ScriptAction = 5
    }
}