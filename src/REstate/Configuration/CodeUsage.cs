﻿namespace REstate.Configuration
{
    public class CodeUsage
    {
        public int CodeUsageId { get; set; }

        public string CodeUsageName { get; set; }

        public string CodeUsageDescription { get; set; }
    }
}