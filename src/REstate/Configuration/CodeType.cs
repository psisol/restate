﻿namespace REstate.Configuration
{
    public class CodeType
    {
        public int CodeTypeId { get; set; }

        public string CodeTypeName { get;set; }

        public string CodeTypeDescription { get; set; }
    }
}
