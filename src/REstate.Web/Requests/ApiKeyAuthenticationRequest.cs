namespace REstate.Web.Requests
{
    public class ApiKeyAuthenticationRequest
    {
        public string ApiKey { get; set; }
    }
}