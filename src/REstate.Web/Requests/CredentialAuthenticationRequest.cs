﻿namespace REstate.Web
{
    public class CredentialAuthenticationRequest
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}