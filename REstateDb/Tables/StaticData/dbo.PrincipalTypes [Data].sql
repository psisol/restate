INSERT [dbo].[PrincipalTypes] ([PrincipalTypeName], [PrincipalTypeDescription]) VALUES (N'Application', N'A application which authenticates using it''s API key.')
INSERT [dbo].[PrincipalTypes] ([PrincipalTypeName], [PrincipalTypeDescription]) VALUES (N'User', N'A user account with password credentials.')
